﻿create database practice default character set utf8;
use practice;

--Q1
create table item_category(
category_id SERIAL,
category_name varchar(256) NOT NULL);

--Q2
create table item(
item_id SERIAL,
item_name varchar(256) NOT NULL,
Item_price int NOT NULL,
category_id int);

--Q3
insert into item_category values(1,'家具');
insert into item_category values(2,'食品');
insert into item_category values(3,'本');

--Q4
insert into item values(1,'堅牢な机',3000,1);
insert into item values(2,'生焼け肉',50,2);
insert into item values(3,'すっきりわかるJava入門',3000,3);
insert into item values(4,'おしゃれな椅子',2000,1);
insert into item values(5,'こんがり肉',500,2);
insert into item values(6,'書き方ドリルSQL',2500,3);
insert into item values(7,'ふわふわのベッド',30000,1);
insert into item values(8,'ミラノ風ドリア',300,2);

--Q5
select item_name, item_price from item
where category_id = 1;

--Q6
select item_name, item_price from item
where item_price >= 1000;

--Q7
select item_name, item_price from item
where item_name like '%肉%';

--Q8
select item_id,item_name,item_price,category_name
from item
inner join item_category
on item.category_id = item_category.category_id;

--Q9
select category_name,SUM(item_price) as total_price
from(
select category_id, category_name, SUM(item_price)
from item_category
left outer join item
on category_id = category_id)
group by category_id, category_name
order by SUM(item_price) DESC
;

